import React, {Component} from 'react'
import Globalize from "globalize";
import Links from "./Links";

var formatterByLocale = {};
const createFormatters = (locale) => {
  return {
    numberFormatter: Globalize(locale).numberFormatter({ maximumFractionDigits: 2 }),
    currencyFormatter: Globalize(locale).currencyFormatter( "PLN" ),
    dateFormatter: Globalize(locale).dateFormatter({ datetime: "medium" }),
    relativeTimeFormatter: Globalize(locale).relativeTimeFormatter( "second"),
    unitFormatter: Globalize(locale).unitFormatter( "mile/hour", { form: "short" } ),
    messageFormatter: Globalize(locale).messageFormatter("greeting")
  }
}

class App extends Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);

    const {locale, locales} = props;
    this.state = {
      locale: locale,
      elapsedTime: 0,
      startTime: new Date()
    }

    locales.forEach((locale) => {
      formatterByLocale[locale] = createFormatters(locale)
    })
  }

  handleChange(locale) {
    this.setState({locale});
  }

  componentDidMount() {
    setInterval(() => {
      const elapsedTime = +((this.state.startTime - new Date()) / 1000).toFixed(0);
      this.setState({ elapsedTime });
    }, 1000); 
  }

  render() {
    const {locales, name} = this.props;
    const {locale} = this.state;
    const {numberFormatter, currencyFormatter,dateFormatter,
      relativeTimeFormatter, unitFormatter, messageFormatter} = formatterByLocale[locale];
    return (
      <div>
        <Links locales={locales} handleChange={this.handleChange}/>
        <br />
        <div>
          <div id="number">{ numberFormatter(12345.6789) }</div>
          <br/>
          <div id="currency">{ currencyFormatter(69900) }</div>
          <br/>
          <div id="date">{ dateFormatter(new Date()) }</div>
          <br/>
          <div id="relative-time">{ relativeTimeFormatter(this.state.elapsedTime) }</div>
          <br/>
          <div id="unit">{ unitFormatter(60) }</div>
          <br/>
          <div id="message">{ messageFormatter({name})}</div>
        </div>
      </div>
    );
  }
}

export default App