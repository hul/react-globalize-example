import React from 'react'

const LocaleLinks = ({locales, handleChange}) => {
    return (
        <div>
            {locales.map((locale) => {
                const onClick = () => { handleChange(locale) }
                return (
                    <a style={{marginRight: 5}} href="#" key={locale} onClick={ onClick } >
                        {locale}
                    </a>
                )
            })}
        </div>        
    )
}

export default LocaleLinks