import React from 'react';
import {render} from 'react-dom';
import App from './App';
import Globalize from "globalize";

import enMessages from '../l10n/en.json';
import plMessages from '../l10n/pl.json';
import frMessages from '../l10n/fr.json';
import esMessages from '../l10n/es.json';

import cldrSubtags from "cldr-data/supplemental/likelySubtags.json"
import cldrNumbering from "cldr-data/supplemental/numberingSystems.json"
import cldrPlurals from "cldr-data/supplemental/plurals.json"
import cldrOrdinals from "cldr-data/supplemental/ordinals.json"
import cldrCurrency from "cldr-data/supplemental/currencyData.json"
import cldrTime from "cldr-data/supplemental/timeData.json"
import cldrWeek from "cldr-data/supplemental/weekData.json"

import plNumbers from "cldr-data/main/pl/numbers.json"
import plCurrencies from "cldr-data/main/pl/currencies.json"
import plCalendar from "cldr-data/main/pl/ca-gregorian.json"
import plTime from "cldr-data/main/pl/timeZoneNames.json"
import plDate from "cldr-data/main/pl/dateFields.json"
import plUnits from "cldr-data/main/pl/units.json"

import esNumbers from "cldr-data/main/es/numbers.json"
import esCurrencies from "cldr-data/main/es/currencies.json"
import esCalendar from "cldr-data/main/es/ca-gregorian.json"
import esTime from "cldr-data/main/es/timeZoneNames.json"
import esDate from "cldr-data/main/es/dateFields.json"
import esUnits from "cldr-data/main/es/units.json"

import frNumbers from "cldr-data/main/fr/numbers.json"
import frCurrencies from "cldr-data/main/fr/currencies.json"
import frCalendar from "cldr-data/main/fr/ca-gregorian.json"
import frTime from "cldr-data/main/fr/timeZoneNames.json"
import frDate from "cldr-data/main/fr/dateFields.json"
import frUnits from "cldr-data/main/fr/units.json"

import enNumbers from "cldr-data/main/en/numbers.json"
import enCurrencies from "cldr-data/main/en/currencies.json"
import enCalendar from "cldr-data/main/en/ca-gregorian.json"
import enTime from "cldr-data/main/en/timeZoneNames.json"
import enDate from "cldr-data/main/en/dateFields.json"
import enUnits from "cldr-data/main/en/units.json"

const locales = ["en", "fr", "es", "pl"];
const messages = {
  en: enMessages,
  es: esMessages,
  fr: frMessages,
  pl: plMessages
}

Globalize.load([cldrSubtags, cldrNumbering, cldrOrdinals, cldrPlurals, cldrTime, cldrWeek, cldrCurrency]);
Globalize.load([plNumbers, plCalendar, plCurrencies, plTime, plUnits, plDate]);
Globalize.load([esNumbers, esCalendar, esCurrencies, esTime, esUnits, esDate]);
Globalize.load([frNumbers, frCalendar, frCurrencies, frTime, frUnits, frDate]);
Globalize.load([enNumbers, enCalendar, enCurrencies, enTime, enUnits, enDate]);
Globalize.loadMessages(messages);

render(<App locale="en" locales={locales} name="Peter Parker"/>, document.getElementById('root'));