var CommonsChunkPlugin = require( "webpack/lib/optimize/CommonsChunkPlugin" );
var HtmlWebpackPlugin = require( "html-webpack-plugin" );
var GlobalizePlugin = require( "globalize-webpack-plugin" );
var nopt = require( "nopt" );

var options = nopt({
	production: Boolean
});

module.exports = {
  entry: options.production ?  {
      main: __dirname + "/app/main.js",
      vendor: [
          "globalize",
          "globalize/dist/globalize-runtime/number",
          "globalize/dist/globalize-runtime/currency",
          "globalize/dist/globalize-runtime/date",
          "globalize/dist/globalize-runtime/message",
          "globalize/dist/globalize-runtime/plural",
          "globalize/dist/globalize-runtime/relative-time",
          "globalize/dist/globalize-runtime/unit"
      ]
  } : __dirname + "/app/main.js",
  debug: !options.production,
	output: {
    path: options.production ? __dirname + "./dist" : __dirname + "/public",
		publicPath: options.production ? "" : "http://localhost:8080/",
		filename: options.production ? "bundle.[hash].js" : "bundle.js"
	},  
  devServer: {
    contentBase: "./public",
    colors: true,
    historyApiFallback: true,
    inline: true
  },
  resolve: {
		extensions: [ "", ".js" ]
	},
  module: {
    loaders: [
      {
        test: /\.json$/,
        loader: "json"
      },
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: 'babel',
        query: {
          presets: ['es2015','react']
        }
      }
    ]
  },
  plugins: [
		new HtmlWebpackPlugin({
			production: options.production,
			template: "./public/index.html"
		}),
		new GlobalizePlugin({
			production: options.production,
			developmentLocale: "en"
		})
	].concat( options.production ? [
		new webpack.optimize.DedupePlugin(),
		new CommonsChunkPlugin( "vendor", "vendor.[hash].js" ),
		new webpack.optimize.UglifyJsPlugin({
			compress: {
				warnings: false
			}
		})
	] : [] )
}